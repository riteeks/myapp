package handler

import (
	"encoding/json"
	"fmt"
	"myapp/contract"
	"myapp/domain"
	"myapp/service"
	"net/http"
)

//CreateUserHandler func
func CreateUserHandler(w http.ResponseWriter, r *http.Request) {
	var userRequest contract.User

	err := json.NewDecoder(r.Body).Decode(&userRequest)
	if err != nil {
		http.Error(w, fmt.Sprintf("error: %s", err.Error()), http.StatusBadRequest)
		return
	}

	user := domain.NewUser(&userRequest)
	err = service.InsertUser(user)
	fmt.Println("check............")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
}
