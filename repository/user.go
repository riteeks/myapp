package repository

import (
	"database/sql"
	"fmt"
	"myapp/db"
	"myapp/domain"
)

const insertQuery = `INSERT into users (age, first_name, last_name) VALUES($1, $2, $3)`

type userRespository struct {
	db *sql.DB
}

func NewUserRepository() *userRespository {
	return &userRespository{
		db: db.InitDB(),
	}
}

func (ur *userRespository) Insert(user *domain.User) error {
	fmt.Println("This is not nil", ur.db)
	_, err := ur.db.Exec(insertQuery, user.Age, user.FirstName, user.LastName)
	if err != nil {
		return fmt.Errorf("[UserRepository] Error while inserting the user data : %s", err)
	}
	return nil
}
