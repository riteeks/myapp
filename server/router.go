package server

import (
	"myapp/handler"
	"net/http"

	"github.com/gorilla/mux"
)

// Router func
func Router() http.Handler {
	router := mux.NewRouter()
	router.HandleFunc("/ping", handler.PingHandler).Methods("GET")
	router.HandleFunc("/createuser", handler.CreateUserHandler).Methods("POST")
	return router
}
