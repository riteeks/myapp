CREATE TABLE users (
    id              serial not null,
    age             int, 
    first_name      varchar,
    last_name       varchar,
    PRIMARY KEY (id)
);
