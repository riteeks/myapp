package migration

import (
	"fmt"
	"myapp/config"

	"github.com/mattes/migrate"
	_ "github.com/mattes/migrate/database/postgres"
	_ "github.com/mattes/migrate/source/file"
)

const migrationPath = "file:///Users/riteek/Golang/src/myapp/migration/queries"

var runner *migrate.Migrate

func Init() {
	connURL := config.Db().ConnString()
	var err error
	runner, err = migrate.New(migrationPath, connURL)
	if err != nil {
		panic(err)
	}
}

func Up() error {
	if err := runner.Up(); err != nil {
		return fmt.Errorf("Error while migration up: %v", err)
	}

	fmt.Println("Migration successful")
	return nil
}

func Down() error {
	if err := runner.Down(); err != nil {
		return fmt.Errorf("Error while migration down: %v", err)
	}

	fmt.Println("Migration successful")
	return nil
}
