.PHONY: all
all: build-deps build fmt vet lint test

GLIDE_NOVENDOR=$(shell glide novendor)
ALL_PACKAGES=$(shell go list ./... | grep -v "vendor")
UNIT_TEST_PACKAGES=$(shell glide novendor | grep -v "featuretest")
TEST_DB_NAME="my_app_test"
DB_NAME="my_app"
DB_USER="postgres"
APP_EXECUTABLE="out/myapp"

setup:
	curl https://glide.sh/get | sh
	go get -u github.com/golang/lint/golint

build-deps:
	glide install

update-deps:
	glide update

compile:
	mkdir -p out/
	go build -o $(APP_EXECUTABLE)

build: build-deps compile fmt vet lint

install:
	go install $(GLIDE_NOVENDOR)

fmt:
	go fmt $(GLIDE_NOVENDOR)

vet:
	go vet $(GLIDE_NOVENDOR)

lint:
	@for p in $(UNIT_TEST_PACKAGES); do \
		echo "==> Linting $$p"; \
		golint $$p | { grep -vwE "exported (var|function|method|type|const) \S+ should have comment" || true; } \
		done

db.setup: db.create 

db.create:
	createdb -O$(DB_USER) -Eutf8 $(DB_NAME)

db.drop:
	dropdb --if-exists -U$(DB_USER) $(DB_NAME)

db.reset: db.drop db.create 

test:
	ENVIRONMENT=test go test $(UNIT_TEST_PACKAGES) -p=1

testdb.setup: testdb.drop testdb.create 

testdb.create: testdb.drop
	createdb -O$(DB_USER) -Eutf8 $(TEST_DB_NAME)

testdb.drop:
	dropdb --if-exists -U$(DB_USER) $(TEST_DB_NAME)

test-coverage:
	@echo "mode: count" > coverage-all.out
	$(foreach pkg, $(ALL_PACKAGES),\
	ENVIRONMENT=test go test -coverprofile=coverage.out -covermode=count $(pkg);\
	tail -n +2 coverage.out >> coverage-all.out;)
	@go tool cover -func=coverage-all.out | grep total | awk '{print $3}'
	@rm -rf coverage-all.out coverage.out

copy-config:
	cp application.yml.sample application.yml
