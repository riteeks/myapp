package app

import "myapp/db"
import "fmt"

func InitApp() {
	db.InitDB()
}

func StopApp() {
	if err := db.Close(); err != nil {
		_ = fmt.Errorf("Error inclosing the db: %v", err)
	}
}
