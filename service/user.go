package service

import (
	"fmt"
	"myapp/domain"
	"myapp/repository"
)

func InsertUser(user *domain.User) error {
	err := repository.NewUserRepository().Insert(user)
	if err != nil {
		return fmt.Errorf("[InsertUser] Error in inserting the user data: %s", err)
	}
	return nil
}
