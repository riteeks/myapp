package config

import (
	"fmt"
	"strconv"

	"github.com/spf13/viper"
)

var appConfig Config

type Config struct {
	port int
	log  LogConfig
	db   DbConfig
}

func Load() {
	viper.SetDefault("APP_PORT", "8080")
	viper.SetDefault("LOG_LEVEL", "error")
	viper.SetConfigName("application")
	viper.AddConfigPath("./")
	viper.AddConfigPath("../")
	viper.AddConfigPath("../../")
	viper.SetConfigType("yaml")

	viper.ReadInConfig()
	viper.AutomaticEnv()

	appConfig = Config{
		port: extractIntValue("APP_PORT"),
		log: LogConfig{
			logLevel: extractStringValue("LOG_LEVEL"),
		},
		db: DbConfig{
			host:     extractStringValue("DATABASE_HOST"),
			port:     extractIntValue("DATABASE_PORT"),
			name:     extractStringValue("DATABASE_NAME"),
			user:     extractStringValue("DATABASE_USER"),
			password: extractStringValue("DATABASE_PASSWORD"),
		},
	}
}

func Port() int {
	return appConfig.port
}

func Log() LogConfig {
	return appConfig.log
}

func Db() DbConfig {
	return appConfig.db
}

func extractStringValue(key string) string {
	checkPresenceOf(key)
	return viper.GetString(key)
}

func extractBoolValue(key string) bool {
	checkPresenceOf(key)
	return viper.GetBool(key)
}

func extractIntValue(key string) int {
	checkPresenceOf(key)
	v, err := strconv.Atoi(viper.GetString(key))
	if err != nil {
		panic(fmt.Sprintf("key %s is not a valid Integer value", key))
	}

	return v
}

func checkPresenceOf(key string) {
	if !viper.IsSet(key) {
		panic(fmt.Sprintf("key %s is not set", key))
	}
}
