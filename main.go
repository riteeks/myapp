package main

import (
	"log"
	"myapp/app"
	"myapp/config"
	"myapp/migration"
	"myapp/server"
	"net/http"
	"os"

	_ "github.com/lib/pq"
	"github.com/urfave/cli"
)

func main() {
	config.Load()
	app.InitApp()

	app := cli.NewApp()
	app.Name = "myapp"
	app.Version = "0.0.1"
	app.Commands = []cli.Command{
		{
			Name:        "start",
			Description: "Run the server",
			Action: func(c *cli.Context) error {
				router := server.Router()
				log.Fatal(http.ListenAndServe(":8080", router))
				return nil
			},
		},
		{
			Name:        "migrate",
			Description: "Run Database migration",
			Action: func(c *cli.Context) error {
				migration.Init()
				return migration.Up()
			},
		},
		{
			Name:        "rollback migration",
			Description: "Rollback latest database migration",
			Action: func(c *cli.Context) error {
				migration.Init()
				return migration.Down()
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		panic(err)
	}

}
