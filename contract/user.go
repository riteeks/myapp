package contract

type User struct {
	ID        int    `json:"id,omitempty"`
	Age       int    `json:"age,omitempty"`
	FirstName string `json:"first_name,omitempty"`
	LastName  string `json:"last_name,omitempty"`
}
