package domain

import (
	"myapp/contract"
)

type User struct {
	ID        int    `json:"id,omitempty"`
	Age       int    `json:"age,omitempty"`
	FirstName string `json:"first_name,omitempty"`
	LastName  string `json:"last_name,omitempty"`
}

func NewUser(user *contract.User) *User {
	return &User{
		ID:        user.ID,
		Age:       user.Age,
		FirstName: user.FirstName,
		LastName:  user.LastName,
	}
}
